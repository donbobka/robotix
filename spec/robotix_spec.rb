# frozen_string_literal: true

require 'robotix'

RSpec.describe Robotix do
  it { expect(described_class::VERSION).not_to be(nil) }
end
