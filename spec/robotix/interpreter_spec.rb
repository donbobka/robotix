# frozen_string_literal: true

require 'robotix/robot'
require 'robotix/interpreter'

RSpec.describe Robotix::Interpreter do
  describe '#run_file' do
    subject(:described_method) { described_class.run_file(io, robot) }

    let(:table) { Robotix::Table.new(width: 5, height: 5) }
    let(:robot) { Robotix::Robot.new(table: table, position: nil, direction: nil) }
    let(:io) { StringIO.new(src) }

    context 'when example 1' do
      let(:src) do
        <<-SRC
        PLACE 0,0,NORTH
        MOVE
        REPORT
        SRC
      end

      it { expect { described_method }.to output("0,1,NORTH\n").to_stdout }
    end

    context 'when example 2' do
      let(:src) do
        <<-SRC
        PLACE 0,0,NORTH
        LEFT
        REPORT
        SRC
      end

      it { expect { described_method }.to output("0,0,WEST\n").to_stdout }
    end

    context 'when example 3' do
      let(:src) do
        <<-SRC
        PLACE 1,2,EAST
        MOVE
        MOVE
        LEFT
        MOVE
        REPORT
        SRC
      end

      it { expect { described_method }.to output("3,3,NORTH\n").to_stdout }
    end
  end
end
