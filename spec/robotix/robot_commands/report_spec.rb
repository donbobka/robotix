# frozen_string_literal: true

require 'robotix/robot_commands/report'
require 'robotix/robot'
require 'robotix/table'

RSpec.describe Robotix::RobotCommands::Report do
  let(:table) { Robotix::Table.new(width: 5, height: 5) }
  let(:position) { [1, 0] }
  let(:direction) { :north }
  let(:robot) { Robotix::Robot.new(table: table, position: position, direction: direction) }

  describe '#call' do
    subject(:described_method) { described_class.new(robot: robot).call }

    context 'when at [1, 0] directed to north' do
      it { expect { described_method }.to output("1,0,NORTH\n").to_stdout }
    end

    context 'without placing robot to table' do
      let(:table) { nil }

      it { expect { described_method }.not_to output.to_stdout }
    end
  end
end
