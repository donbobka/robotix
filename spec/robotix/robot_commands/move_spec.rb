# frozen_string_literal: true

require 'robotix/robot_commands/move'
require 'robotix/robot'
require 'robotix/table'

RSpec.describe Robotix::RobotCommands::Move do
  let(:table) { Robotix::Table.new(width: 5, height: 5) }
  let(:position) { [0, 0] }
  let(:direction) { :north }
  let(:robot) { Robotix::Robot.new(table: table, position: position, direction: direction) }

  describe '#call' do
    subject(:described_method) { described_class.new(robot: robot).call }

    context 'when at [1, 0] directed to north' do
      let(:position) { [1, 0] }
      let(:direction) { :north }

      it { expect(described_method.to_h).to include(position: [1, 1]) }
    end

    context 'without placing robot to table' do
      let(:table) { nil }

      it('ignores call') { expect(described_method.to_h).to eq(robot.to_h) }
    end

    context 'with at [1, 0] directed to east' do
      let(:position) { [1, 0] }
      let(:direction) { :east }

      it { expect(described_method.to_h).to include(position: [2, 0]) }
    end

    context 'with direction to west near west border' do
      let(:position) { [0, 1] }
      let(:direction) { :west }

      it('ignores call') { expect(described_method.to_h).to eq(robot.to_h) }
    end

    context 'with direction to east near east border' do
      let(:position) { [4, 0] }
      let(:direction) { :east }

      it('ignores call') { expect(described_method.to_h).to eq(robot.to_h) }
    end
  end
end
