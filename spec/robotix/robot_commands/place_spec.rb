# frozen_string_literal: true

require 'robotix/robot_commands/place'
require 'robotix/robot'
require 'robotix/table'

RSpec.describe Robotix::RobotCommands::Place do
  let(:table) { Robotix::Table.new(width: 5, height: 5) }
  let(:robot) { Robotix::Robot.new(table: table, position: nil, direction: nil) }

  describe '#call' do
    subject(:described_method) { described_class.new(robot: robot, position: position, direction: direction).call }

    let(:position) { [1, 0] }
    let(:direction) { :north }

    it { expect(described_method.to_h).to include(position: position, direction: direction) }
    it { expect(described_method.placed?).to be(true) }

    context 'when position is out of table' do
      let(:position) { [10, 10] }

      it { expect(described_method.placed?).to be(false) }
    end
  end
end
