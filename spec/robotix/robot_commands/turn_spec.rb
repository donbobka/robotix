# frozen_string_literal: true

require 'robotix/robot_commands/turn'
require 'robotix/robot'
require 'robotix/table'

RSpec.describe Robotix::RobotCommands::Turn do
  let(:table) { Robotix::Table.new(width: 5, height: 5) }
  let(:position) { [1, 0] }
  let(:direction) { :north }
  let(:robot) { Robotix::Robot.new(table: table, position: position, direction: direction) }

  describe '#call' do
    subject(:described_method) { described_class.new(robot: robot, direction: turn_direction).call }

    context 'when turn to right and directed to north' do
      let(:direction) { :north }
      let(:turn_direction) { :right }

      it { expect(described_method.to_h).to include(direction: :east) }
    end

    context 'when turn to right and directed to west' do
      let(:direction) { :west }
      let(:turn_direction) { :right }

      it { expect(described_method.to_h).to include(direction: :north) }
    end

    context 'when turn to left and directed to north' do
      let(:direction) { :north }
      let(:turn_direction) { :left }

      it { expect(described_method.to_h).to include(direction: :west) }
    end

    context 'when turn to left and directed to west' do
      let(:direction) { :west }
      let(:turn_direction) { :left }

      it { expect(described_method.to_h).to include(direction: :south) }
    end

    context 'without placing robot to table' do
      let(:table) { nil }
      let(:turn_direction) { :left }

      it('ignores call') { expect(described_method.to_h).to eq(robot.to_h) }
    end
  end
end
