# frozen_string_literal: true

require 'robotix/parser'

RSpec.describe Robotix::Parser do
  subject(:parser) { described_class.new }

  let(:good_src) do
    %(PLACE 1,2,EAST
      MOVE

      MOVE
      LEFT
      RIGHT
      MOVE


      REPORT
    )
  end
  let(:good_commands) do
    [{ klass: Robotix::RobotCommands::Place, args: { position: [1, 2], direction: :east } },
     { klass: Robotix::RobotCommands::Move },
     { klass: Robotix::RobotCommands::Move },
     { klass: Robotix::RobotCommands::Turn, args: { direction: :left } },
     { klass: Robotix::RobotCommands::Turn, args: { direction: :right } },
     { klass: Robotix::RobotCommands::Move },
     { klass: Robotix::RobotCommands::Report }]
  end
  let(:src) { good_src }

  it { expect(parser.parse(src)).to eq(good_commands) }

  context 'when source has a syntax error' do
    let(:src) { good_src.gsub('MOVE', 'MOV') }

    it { expect { parser.parse(src) }.to raise_error(Racc::ParseError) }
  end
end
