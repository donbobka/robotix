# frozen_string_literal: true

require 'robotix/table'

RSpec.describe Robotix::Table do
  let(:width) { 5 }
  let(:height) { 5 }
  let(:table) { described_class.new(width: width, height: height) }

  describe '#valid_move?' do
    subject { table.valid_move?(from, to) }

    context 'when move inside of table' do
      let(:from) { [2, 2] }
      let(:to) { [3, 2] }

      it { is_expected.to be(true) }
    end

    context 'when don\'t move' do
      let(:from) { [2, 2] }
      let(:to) { [2, 2] }

      it { is_expected.to be(true) }
    end

    context 'when move across the zero border' do
      let(:from) { [0, 1] }
      let(:to) { [-1, 1] }

      it { is_expected.to be(false) }
    end

    context 'when move across the border' do
      let(:from) { [width - 1, height - 1] }
      let(:to) { [width, height] }

      it { is_expected.to be(false) }
    end

    context 'when move by diagonal' do
      let(:from) { [0, 0] }
      let(:to) { [1, 1] }

      it { is_expected.to be(false) }
    end
  end
end
