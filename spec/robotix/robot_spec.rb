# frozen_string_literal: true

require 'robotix/table'
require 'robotix/robot'

RSpec.describe Robotix::Robot do
  let(:table) { Robotix::Table.new(width: 5, height: 5) }
  let(:initial_position) { [0, 0] }
  let(:initial_direction) { :north }
  let(:unplaced_robot) { described_class.new(table: nil, position: nil, direction: nil) }
  let(:placed_robot) { described_class.new(table: table, position: initial_position, direction: initial_direction) }
  let(:robot) { placed_robot }

  describe '#placed?' do
    subject(:method) { robot.placed? }

    it { is_expected.to be(true) }

    context 'without placing robot to table' do
      let(:robot) { unplaced_robot }

      it { is_expected.to be(false) }
    end

    context 'with placing robot to table without position' do
      let(:robot) { placed_robot }
      let(:initial_position) { nil }

      it { is_expected.to be(false) }
    end

    context 'with placing robot to table without direction' do
      let(:robot) { placed_robot }
      let(:initial_direction) { nil }

      it { is_expected.to be(false) }
    end
  end
end
