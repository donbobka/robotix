# frozen_string_literal: true

module Robotix
  autoload :CLI,           'robotix/cli'
  autoload :Interpreter,   'robotix/interpreter'
  autoload :Parser,        'robotix/parser'
  autoload :Robot,         'robotix/robot'
  autoload :RobotCommands, 'robotix/robot_commands'
  autoload :Table,         'robotix/table'
  autoload :VERSION,       'robotix/version'
end
