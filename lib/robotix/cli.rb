# frozen_string_literal: true

require 'thor'

require 'robotix/interpreter'
require 'robotix/parser'
require 'robotix/robot'
require 'robotix/table'

module Robotix
  class CLI < Thor
    desc 'version', 'Robotix version'
    def version
      require_relative 'version'
      puts "v#{Robotix::VERSION}"
    end
    map %w[--version -v] => :version

    desc 'start FILE [SIZE]', 'Run robotix script from FILE on table SIZE x SIZE'
    def start(path, size = '5')
      size =~ /\d+/ || begin
        STDERR.puts('Table size must be integer > 0')
        exit(-1)
      end

      File.file?(path) || begin
        STDERR.puts("File #{path} not found")
        exit(-1)
      end
      file = File.open(path, 'r')
      Robotix::Interpreter.run_file(file,
                                    Robotix::Robot.new(table: Robotix::Table.new(width: size.to_i,
                                                                                 height: size.to_i),
                                                       position: nil, direction: nil))
    rescue Racc::ParseError => e
      STDERR.puts("Syntax error in #{file}: #{e}")
      exit(-1)
    ensure
      file&.close
    end
  end
end
