# frozen_string_literal: true

module Robotix
  class Interpreter
    attr_reader :robot
    attr_reader :commands

    def initialize(robot:, commands:)
      @robot = robot
      @commands = commands
    end

    def call
      commands.each do |cmd|
        run_command(cmd)
      end

      robot
    end

    def run_command(command)
      @robot = command.fetch(:klass).new({ robot: robot }.merge(command.fetch(:args, {}))).call
    end

    def self.run_file(io, robot)
      Robotix::Interpreter.new(robot: robot,
                               commands: Robotix::Parser.new.parse(io.read)).call
    end
  end
end
