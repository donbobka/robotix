class Robotix::Parser

start input

rule
  input: /* empty */ { result = [] }
       | input line { result = val[1].nil? ? val[0] : val[0].push(val[1]) }
       ;
  line: "\n" { result = nil }
      | command "\n" { result = val[0] }
  command: 'PLACE' NUMBER ',' NUMBER ',' direction { result = { klass: RobotCommands::Place,
                                                                args: { position: [val[1], val[3]],
                                                                        direction: val[5] } } }
         | 'MOVE'   { result = { klass: RobotCommands::Move } }
         | 'LEFT'   { result = { klass: RobotCommands::Turn, args: { direction: :left } } }
         | 'RIGHT'  { result = { klass: RobotCommands::Turn, args: { direction: :right } } }
         | 'REPORT' { result = { klass: RobotCommands::Report } }
         ;
  direction: DIRECTION { result = val[0].downcase.to_sym }

---- header ----

# frozen_string_literal: true

require 'strscan'

require 'robotix/robot_commands'

---- inner ----

def tokenize(str)
  tokens = []

  s = StringScanner.new(str)
  until s.eos?
    tokens << case
                when s.scan(/^\n/) then [s[0], s[0]]
                when s.skip(/^\s+/)
                when s.scan(/^\d+/) then [:NUMBER, s[0].to_i]
                when s.scan(/^(PLACE|MOVE|LEFT|RIGHT|REPORT)/) then [s[0], s[0]]
                when s.scan(/^(NORTH|EAST|SOUTH|WEST)/) then [:DIRECTION, s[0]]
                else [c = s.getch, c]
              end
  end

  tokens.compact
end

def parse(str)
  @tokens = tokenize(str)
  do_parse
end

def next_token
  @tokens.shift
end
