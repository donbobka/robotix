# frozen_string_literal: true

require 'robotix/types'

module Robotix
  class Table < Dry::Struct
    attribute :width, Types::Strict::Int.constrained(gteq: 0)
    attribute :height, Types::Strict::Int.constrained(gteq: 0)

    def valid_move?(from, to)
      # Check move across the border
      return false if to.any?(&:negative?)
      return false if to[0] >= width || to[1] >= height
      # Check diagonal move
      return false if from[0] != to[0] && from[1] != to[1]

      true
    end
  end
end
