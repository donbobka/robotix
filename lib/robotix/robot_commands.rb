# frozen_string_literal: true

module Robotix
  module RobotCommands
    autoload :Move,   'robotix/robot_commands/move'
    autoload :Place,  'robotix/robot_commands/place'
    autoload :Report, 'robotix/robot_commands/report'
    autoload :Turn,   'robotix/robot_commands/turn'
  end
end
