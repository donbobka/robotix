# frozen_string_literal: true

require 'dry-struct'

require 'robotix/table'
require 'robotix/types'

module Robotix
  class Robot < Dry::Struct
    DIRECTIONS_ORDER = %i[north east south west].freeze
    Directions = Types::Strict::Symbol.enum(*DIRECTIONS_ORDER)
    Position = Types::Strict::Array.of(Types::Strict::Int).constrained(size: 2)

    attribute :table, Robotix::Table.optional
    attribute :position, Position.optional
    attribute :direction, Directions.optional

    def placed?
      !(table.nil? || position.nil? || direction.nil?)
    end
  end
end
