# frozen_string_literal: true

module Robotix
  module RobotCommands
    class Place
      attr_reader :robot
      attr_reader :position
      attr_reader :direction

      def initialize(robot:, position:, direction:)
        @robot = robot
        @position = position
        @direction = direction
      end

      def call
        return robot unless robot.table&.valid_move?(position, position)

        robot.new(position: position, direction: direction)
      end
    end
  end
end
