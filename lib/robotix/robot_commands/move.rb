# frozen_string_literal: true

require 'matrix'

module Robotix
  module RobotCommands
    class Move
      SIGHT_VECTORS = { north: Vector[0, 1],
                        east:  Vector[1, 0],
                        south: Vector[0, -1],
                        west:  Vector[-1, 0] }.freeze

      attr_reader :robot

      def initialize(robot:)
        @robot = robot
      end

      def call
        return robot unless robot.placed? && robot.table.valid_move?(robot.position, new_position)

        robot.new(position: new_position)
      end

      def new_position
        @new_position ||= (Vector.elements(robot.position) + SIGHT_VECTORS[robot.direction]).to_a
      end
    end
  end
end
