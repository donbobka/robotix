# frozen_string_literal: true

module Robotix
  module RobotCommands
    class Report
      attr_reader :robot

      def initialize(robot:)
        @robot = robot
      end

      def call
        return robot unless robot.placed?

        puts "#{robot.position.join(',')},#{robot.direction.to_s.upcase}"
        robot
      end
    end
  end
end
