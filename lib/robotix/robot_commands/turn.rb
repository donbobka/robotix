# frozen_string_literal: true

require 'robotix/robot'

module Robotix
  module RobotCommands
    class Turn
      DIRECTIONS_ORDER = Robot::DIRECTIONS_ORDER

      attr_reader :robot
      attr_reader :direction

      def initialize(robot:, direction:)
        @robot = robot
        @direction = direction
      end

      def call
        return robot unless robot.placed?

        sign = direction == :right ? 1 : -1
        new_direction = DIRECTIONS_ORDER[(DIRECTIONS_ORDER.index(robot.direction) + sign) % DIRECTIONS_ORDER.size]

        robot.new(direction: new_direction)
      end
    end
  end
end
